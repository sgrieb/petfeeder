# README #

### What is this repository for? ###

The purpose of this project is to create a simple webserver that can be run on a raspberry pi.  

The Python microframework Flask will accomplish this.

Some goals in no particular order:

-  maintain a front end which is responsive so it can be used from any device (bootstrap)
-  expose it to the outside world via port forwarding/static IP or something of that sort
-  hook up a stepper motor and food container to allow food distribution on command via web interface
-  utilize raspi camera to allow video streaming at device location

### How do I get set up? ###
TBD